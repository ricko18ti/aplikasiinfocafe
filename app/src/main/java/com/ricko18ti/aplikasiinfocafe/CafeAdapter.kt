package com.ricko18ti.aplikasiinfocafe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

class CafeAdapter(
    val CafeContext: Context,
    val layoutResId: Int,
    val cafeList: List<Cafe>
): ArrayAdapter<Cafe>(CafeContext, layoutResId, cafeList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(CafeContext)
        val view: View = layoutInflater.inflate(layoutResId, null)

        val namacafe: TextView = view.findViewById(R.id.nama_cafe)
        val alamat: TextView = view.findViewById(R.id.alamat_cafe)
        val keterangan: TextView = view.findViewById(R.id.keterangan_cafe)
        //val latitude: TextView = view.findViewById(R.id.koordinat_x)
        //val longtitude: TextView = view.findViewById(R.id.koordinat_y)

        val cafe = cafeList[position]

        namacafe.text = "Nama : " + cafe.nama_cafe
        alamat.text = "Alamat : " + cafe.alamat_cafe
        keterangan.text = "Keterangan : " + cafe.keterangan
        //latitude.text = "Latitude : " + cafe.latitude
        //longtitude.text = "Longtitude : " + cafe.latitude

        return view
    }
}