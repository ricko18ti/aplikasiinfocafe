package com.ricko18ti.aplikasiinfocafe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.google.firebase.database.*

class UserCafeFragment : Fragment() {

    private lateinit var listData: ListView
    private lateinit var ref: DatabaseReference
    private lateinit var cafeList: MutableList<Cafe>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_cafe, container, false)
        ref = FirebaseDatabase.getInstance().getReference("cafe")
        listData = view.findViewById(R.id.list_cafe)
        cafeList = mutableListOf()

        ref.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()) {
                    cafeList.clear()
                    for (s in snapshot.children) {
                        val cafe = s.getValue(Cafe::class.java)
                        if (cafe != null) {
                            cafeList.add(cafe)
                        }
                    }

                    val adapter = context?.let {
                        CafeAdapter(
                            it,
                            R.layout.layout_list_cafe, cafeList)
                    }
                    listData.adapter = adapter
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

        // Inflate the layout for this fragment
        return view
    }
}