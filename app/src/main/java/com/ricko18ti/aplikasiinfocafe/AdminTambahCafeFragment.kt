package com.ricko18ti.aplikasiinfocafe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_admin_tambah_cafe.view.*

class AdminTambahCafeFragment : Fragment() {

    private lateinit var namacafe: EditText
    private lateinit var alamatcafe: EditText
    private lateinit var keterangan: EditText
    private lateinit var latitude: EditText
    private lateinit var longitude: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_admin_tambah_cafe, container, false)

        namacafe = view.findViewById(R.id.nama_cafe)
        alamatcafe = view.findViewById(R.id.alamat_cafe)
        keterangan = view.findViewById(R.id.keterangan_cafe)
        latitude = view.findViewById(R.id.koordinat_x)
        longitude = view.findViewById(R.id.koordinat_y)

        view.btn_tambah.setOnClickListener{
            insert()
        }
        return view
    }

    private fun insert(){
        var ref: DatabaseReference = FirebaseDatabase.getInstance().getReference("cafe")

        val nama_cafe = namacafe.text.toString().trim()
        val alamat_cafe = alamatcafe.text.toString()
        val keterangan_cafe = keterangan.text.toString()
        val koordinat_x = latitude.text.toString()
        val koordinat_y = longitude.text.toString()

        if (nama_cafe.isEmpty() or alamat_cafe.isEmpty() or keterangan_cafe.isEmpty() or koordinat_x.isEmpty() or koordinat_y.isEmpty()) {
            Toast.makeText(context, "Data tidak boleh kosong", Toast.LENGTH_SHORT) //getActivity() atau getApplicationContext()
                .show()
            return
        }

        val id_cafe = ref.push().key
        val cafe = Cafe(id_cafe!!, nama_cafe, alamat_cafe, keterangan_cafe, koordinat_x, koordinat_y)
        ref.child(id_cafe).setValue(cafe).addOnCompleteListener {
            Toast.makeText(context, "Data berhasil ditambahkan", Toast.LENGTH_SHORT) //getActivity() atau getApplicationContext()
                .show()
        }
    }
}