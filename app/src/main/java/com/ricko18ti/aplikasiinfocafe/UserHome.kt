package com.ricko18ti.aplikasiinfocafe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.user_home.*

class UserHome: AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var caricafe: UserCafeFragment
    lateinit var tentang_kami:Fragment_tentang_kami
    //tambahkan fragment dari menu di sini

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_home)

        toggle = ActionBarDrawerToggle(this, user_drawer, R.string.open, R.string.close)
        user_drawer.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        user_nav.setNavigationItemSelectedListener(this)
        //halaman pertama
        //tentangkami = Fragment_tentang_kami()
        //supportFragmentManager
        //    .beginTransaction()
        //    .replace(R.id.user_layout, tentangkami)
        //    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        //    .commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_cari_cafe -> {
                caricafe = UserCafeFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.user_layout, caricafe)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.menu_info -> {
                tentang_kami = Fragment_tentang_kami()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.user_layout, tentang_kami)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.menu_keluar -> {
                this.finish()
            }
        }
        user_drawer.closeDrawers()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}