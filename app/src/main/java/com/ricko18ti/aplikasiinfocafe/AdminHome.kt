package com.ricko18ti.aplikasiinfocafe

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.admin_home.*

class AdminHome: AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var tambahcafe: AdminTambahCafeFragment
    lateinit var listcafe: AdminCafeFragment
    lateinit var infocafe: AdminCafeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_home)

        toggle = ActionBarDrawerToggle(this, admin_drawer, R.string.open, R.string.close)
        admin_drawer.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        admin_nav.setNavigationItemSelectedListener(this)

        infocafe = AdminCafeFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.admin_layout, infocafe)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.admin_cafe-> {
                tambahcafe = AdminTambahCafeFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.admin_layout, tambahcafe)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.admin_list_cafe -> {
                listcafe = AdminCafeFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.admin_layout, listcafe)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.keluar-> {
                this.finish()
            }
        }
        admin_drawer.closeDrawers()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}