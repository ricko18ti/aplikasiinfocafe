package com.ricko18ti.aplikasiinfocafe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_utama.view.*
import android.widget.Toast

class UtamaFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_utama, container, false)

        view.btn_admin.setOnClickListener {
            findNavController().navigate(R.id.action_utamaFragment_to_loginFragment)
        }
        view.btn_user.setOnClickListener {
            findNavController().navigate(R.id.action_utamaFragment_to_userHome)
        }
        return view
    }
}