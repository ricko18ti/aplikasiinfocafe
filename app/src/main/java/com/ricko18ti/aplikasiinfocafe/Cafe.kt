package com.ricko18ti.aplikasiinfocafe

data class Cafe(
    val id_cafe: String,
    val nama_cafe: String,
    val alamat_cafe: String,
    val keterangan: String,
    val latitude: String,
    val longtitude: String,

){
    constructor(): this("","","","","","")
}